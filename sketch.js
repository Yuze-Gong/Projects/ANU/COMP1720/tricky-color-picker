// this is the variable your picker should change
var pickedColor = 0;
var currentX = 0
var currentY = 0
var orientation = 0
var stepsToGo = 0

function setup() {
  noStroke()
  createCanvas(windowWidth, windowHeight);
  initConstants()
}

var gap
var edge
var radius
var palette
var innerOffset
var verticalOffset
var horizontalOffset
const gridNum = 8

function initConstants() {
  edge = min(windowWidth, windowHeight)
  gap = edge * 0.9 / gridNum
  radius = gap * 0.9
  innerOffset = gap * 0.5 + edge * 0.05
  palette = paletteGenerator(pow(gridNum, 2))
  offset = function (a, b) { horizontalOffset = a; verticalOffset = b }
  distance = abs(windowWidth - windowHeight) / 2
  if (windowWidth >= windowHeight)
    offset(distance, 0)
  else
    offset(0, distance)
}

function draw() {
  // your colour picker code goes here

  // display the currently "picked" colour in the bottom-right
  // this is just here for the template - you can display it however you like
  // see the FAQ for details
  update()
  noLoop()
}

function left() { currentX -= 1 }
function right() { currentX += 1 }
function up() { currentY -= 1 }
function down() { currentY += 1 }

triggers = [
  [up, down, left, right],
  [down, up, right, left],
  [right, left, up, down],
  [left, right, down, up]
]

function keyPressed() {
  trigger = triggers[orientation]
  if (keyCode === UP_ARROW)
    trigger[0]()
  if (keyCode === DOWN_ARROW)
    trigger[1]()
  if (keyCode === LEFT_ARROW)
    trigger[2]()
  if (keyCode === RIGHT_ARROW)
    trigger[3]()
  update()
}

function update() {
  if (stepsToGo == 0) {
    stepsToGo = int(random(6, 10))
    orientation = int(random(4))
    console.log(orientation)
  }
  stepsToGo -= 1
  background(255)
  drawBackground()
  drawPalette()
  drawMarker()
  drawIndicator()
}

function drawIndicator() {
  w = radius * 0.5
  bottomGap = edge * 0.04
  fill(getColor(currentX, currentY))
  vert = function(top, bottom) {
    triangle(
      windowWidth / 2,     top,
      windowWidth / 2 + w, bottom,
      windowWidth / 2 - w, bottom
    )
  }
  hori = function(top, bottom) {
    triangle(
      top,    windowHeight / 2,
      bottom, windowHeight / 2 + w,
      bottom, windowHeight / 2 - w 
    )
  }
  if (orientation == 0)
    vert(verticalOffset, verticalOffset + bottomGap)
  if (orientation == 1)
    vert(windowHeight - verticalOffset, windowHeight - verticalOffset - bottomGap)
  if (orientation == 2)
    hori(horizontalOffset, horizontalOffset + bottomGap)
  if (orientation == 3)
    hori(windowWidth - horizontalOffset, windowWidth - horizontalOffset - bottomGap)
}

function drawBackground() {
  fill(getColor(currentX, currentY))
  square(horizontalOffset + edge * 0.05, verticalOffset + edge * 0.05, edge * 0.9)
}

function square(x, y, w, r) {
  rect(x, y, w, w, r)
}

function drawMarker() {
  c = getColor(currentX, currentY)
  fill(255 - red(c), 255 - green(c), 255 - blue(c))
  xPos = horizontalOffset + innerOffset + mod(currentX) * gap
  yPos = verticalOffset + innerOffset + mod(currentY) * gap
  ellipse(xPos, yPos, radius * 0.1)
}

function drawPalette() {
  for (col = 0; col < gridNum; col++) {
    for (row = 0; row < gridNum; row++) {
      drawGrid(row, col)
    }
  }
}

function drawGrid(r, c) {
  xPos = horizontalOffset + innerOffset + r * gap
  yPos = verticalOffset + innerOffset + c * gap
  fill(getColor(r, c))
  ellipse(xPos, yPos, radius)
}

function getColor(r, c) {
  return palette[mod(r) * gridNum + mod(c)]
}

function mod(n) {
  l = gridNum
  return ((n % l) + l) % l
}

function paletteGenerator(n) {
  colorList = []
  for (i = 0; i < n; i++) {
    colorList[i] = colorGenerator()
  }
  return colorList
}

function colorGenerator() {
  ran = function () { return int(random(195)) }
  return rgb(ran(), ran(), ran())
}

function rgb(r, g, b) {
  return `rgb(${r}, ${g}, ${b})`
}